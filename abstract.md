# Title

LFortran: Interactive LLVM-based Fortran Compiler for Modern Architectures

# Authors

Ondřej Čertík, ondrej@certik.us, Los Alamos National Laboratory  
Nikhil Maan, nikhilmaan22@gmail.com, Amity University, India  
Ankit Pandey, pandeyan@grinnell.edu, Grinnell College  
Milan Curcic, caomaco@gmail.com, University of Miami  
Peter Brady, ptb@lanl.gov, Los Alamos National Laboratory  
Zach Jibben, zjibben@lanl.gov, Los Alamos National Laboratory  
Neil Carlson, nnc@lanl.gov, Los Alamos National Laboratory  
Rohit Goswami, rog32@hi.is, Science Institute, University of Iceland, VR-III, 107, Reykjavik, Iceland  

# Abstract

We are developing a modern open-source Fortran compiler called LFortran
(https://lfortran.org/) that can execute user's code interactively in Jupyter to
allow exploratory work (much like Python, MATLAB or Julia) as well as compile to
binaries with the goal to run user's code on modern architectures such as
multi-core CPUs and GPUs, which is an essential requirement for wider Fortran
adoption that current Fortran compilers do not address well. Live demo in a
Jupyter notebook will be shown. The compiler itself is written in C++ for
robustness and speed with optional Python wrappers. It parses Fortran code to an
Abstract Syntax Tree (AST) and transforms it to an Abstract Semantic
Representation (ASR). LFortran has several backends that transform the ASR to
machine code via LLVM, or to C++, or to provide automatic Python wrappers and
more backends are planned.
